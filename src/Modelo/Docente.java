
package Modelo;

/**
 *
 * @author quier
 */
public class Docente {
    
    private int numDocente;
    private String nombre;
    private String domicilio;
    private int nivel;
    private float pagoBasePorHora;
    private int horasImpartidas;

    public Docente() {
        this.numDocente = 0;
        this.nombre = "";
        this.domicilio = "";
        this.nivel = 0;
        this.pagoBasePorHora = 0;
        this.horasImpartidas = 0;
    }
    
    public Docente(Docente otro) {
        this.numDocente = otro.numDocente;
        this.nombre = otro.nombre;
        this.domicilio = otro.domicilio;
        this.nivel = otro.nivel;
        this.pagoBasePorHora = otro.pagoBasePorHora;
        this.horasImpartidas = otro.horasImpartidas;
    }

    public Docente(int numDocente, String nombre, String domicilio, int nivel, float pagoBasePorHora, int horasImpartidas) {
        this.numDocente = numDocente;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.nivel = nivel;
        this.pagoBasePorHora = pagoBasePorHora;
        this.horasImpartidas = horasImpartidas;
    }
    
    public int getNumDocente() {
        return numDocente;
    }

    public void setNumDocente(int numDocente) {
        this.numDocente = numDocente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public float getPagoBasePorHora() {
        return pagoBasePorHora;
    }

    public void setPagoBasePorHora(float pagoBasePorHora) {
        this.pagoBasePorHora = pagoBasePorHora;
    }

    public int getHorasImpartidas() {
        return horasImpartidas;
    }

    public void setHorasImpartidas(int horasImpartidas) {
        this.horasImpartidas = horasImpartidas;
    }
    
    public float calcularPago() {
        if(this.nivel == 1)
            return ((this.pagoBasePorHora * (float)(1.3)) * (float)horasImpartidas);
        else if(this.nivel == 2)
            return ((this.pagoBasePorHora * (float)(1.5)) * (float)horasImpartidas);
        else if(this.nivel == 3)
            return ((this.pagoBasePorHora * (float)(2.0)) * (float)horasImpartidas);
        return 0;
    }
    
    public float calcularImpuesto() {
        return calcularPago() * (float)(.16);
    }
    
    public float calcularBono(int hijos)
    {
        //System.out.println(calcularPago() * (float)(.05));
        //System.out.println(hijos);
        if(hijos == 0)
            return 0;
        if(hijos == 1 || hijos == 2)
            return calcularPago() * (float)(.05);
        else if(hijos <= 5)
            return calcularPago() * (float)(.10);
        else
            return calcularPago() * (float)(.20);
    }
    
}
