
package Controlador;
import Modelo.Docente;
import Vista.dlgDocente;
// Se importan listeners
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
// Se importan ayudas visuales
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author quier
 */
// Se terminó el proyecto :D
public class Controlador implements ActionListener {

    private Docente docente;
    private dlgDocente vista;
    private int hijos;
    
    public Controlador(Docente docente, dlgDocente vista) {
        this.docente = docente;
        this.vista = vista;
        
        vista.btnNuevo.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
    }
    
    public static void main(String[] args) {
        Docente docente = new Docente();
        dlgDocente vista = new dlgDocente(new JFrame(), true);
        
        Controlador contra = new Controlador(docente, vista);
        contra.iniciarVista();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == vista.btnNuevo) {
            // Unlock save and inputs
            vista.btnGuardar.setEnabled(true);
            vista.txtNombre.setEnabled(true);
            vista.txtDomicilio.setEnabled(true);
            vista.comboBoxNivel.setEnabled(true);
            vista.txtNumDocente.setEnabled(true);
            vista.txtPagoBasePorHora.setEnabled(true);
            vista.txtHorasImpartidas.setEnabled(true);
            vista.spinnerHijos.setEnabled(true);

        }
        
        if(e.getSource() == vista.btnGuardar) {
            try {
                if(Integer.parseInt(vista.txtNumDocente.getText()) <= 0)
                    throw new IllegalArgumentException("ID no puede ser negativo o cero...");
                if(Integer.parseInt(vista.txtHorasImpartidas.getText()) <= 0)
                    throw new IllegalArgumentException("HORAS IMPARTIDAS no puede ser negativo o cero...");
                if(Float.parseFloat(vista.txtPagoBasePorHora.getText()) < 0)
                    throw new IllegalArgumentException("PAGO BASE POR HORA no puede ser negativo...");
                if(Integer.parseInt(vista.spinnerHijos.getValue().toString()) < 0)
                    throw new IllegalArgumentException("HIJOS no puede ser negativo o String...");
                docente.setNumDocente(Integer.parseInt(vista.txtNumDocente.getText()));
                docente.setPagoBasePorHora(Float.parseFloat(vista.txtPagoBasePorHora.getText()));
                docente.setHorasImpartidas(Integer.parseInt(vista.txtHorasImpartidas.getText()));
                hijos = Integer.parseInt(vista.spinnerHijos.getValue().toString());
            }
            catch(NumberFormatException ex) {
                JOptionPane.showMessageDialog(vista, "Error: " + ex.getMessage());
                return;
            }
            catch(IllegalArgumentException ex2) {
                JOptionPane.showMessageDialog(vista, "Error: " + ex2.getMessage());
                return;
            }
            
            docente.setNombre(vista.txtNombre.getText());
            docente.setDomicilio(vista.txtDomicilio.getText());
            if(vista.comboBoxNivel.getSelectedIndex() == 0)
                docente.setNivel(1);
            else if(vista.comboBoxNivel.getSelectedIndex() == 1)
                docente.setNivel(2);
            else if(vista.comboBoxNivel.getSelectedIndex() == 2)
                docente.setNivel(3);
            limpiar();
            vista.btnMostrar.setEnabled(true);
            
        }
        
        if(e.getSource() == vista.btnMostrar) {
            vista.txtNumDocente.setText(Integer.toString(docente.getNumDocente()));
            vista.txtNombre.setText(docente.getNombre());
            vista.txtDomicilio.setText(docente.getDomicilio());
            if(docente.getNivel() == 1)
                vista.comboBoxNivel.setSelectedIndex(0);
            else if(docente.getNivel() == 2)
                vista.comboBoxNivel.setSelectedIndex(1);
            else if(docente.getNivel() == 3)
                vista.comboBoxNivel.setSelectedIndex(2);
            vista.txtPagoBasePorHora.setText(Float.toString(docente.getPagoBasePorHora()));
            vista.txtHorasImpartidas.setText(Integer.toString(docente.getHorasImpartidas()));
            vista.spinnerHijos.setValue(hijos);
            // Se calculan las labels
            vista.labelPagoPorHorasImpartidas.setText(Float.toString(docente.calcularPago()));
            vista.labelBono.setText(Float.toString(docente.calcularBono(hijos)));
            //System.out.println(vista.spinnerHijos.getValue().toString());
            vista.labelDescuentoImpuesto.setText(Float.toString(docente.calcularImpuesto()));
            vista.labelTotalPagar.setText(Float.toString(docente.calcularPago() + Float.parseFloat(vista.labelBono.getText()) - docente.calcularImpuesto()));
        }
        
        if(e.getSource() == vista.btnLimpiar) {
            hijos = Integer.parseInt(vista.spinnerHijos.getValue().toString());
            limpiar();
        }
        
        if(e.getSource() == vista.btnCancelar) {
            limpiar();
            hijos = 0;
            // Unlock save and inputs
            vista.btnGuardar.setEnabled(false);
            vista.btnMostrar.setEnabled(false);
            vista.txtNombre.setEnabled(false);
            vista.txtDomicilio.setEnabled(false);
            vista.comboBoxNivel.setEnabled(false);
            vista.txtNumDocente.setEnabled(false);
            vista.txtPagoBasePorHora.setEnabled(false);
            vista.txtHorasImpartidas.setEnabled(false);
            vista.spinnerHijos.setEnabled(false);
            
            vista.spinnerHijos.setValue(0);
            
        }
        
        if(e.getSource() == vista.btnCerrar) {
            int option = JOptionPane.showConfirmDialog(vista, "¿Seguro que quieres salir?", "Cerrar aplicación", JOptionPane.YES_NO_OPTION);
            if(option == JOptionPane.YES_OPTION) {
                vista.dispose();
                System.exit(0);
            }
        }
        
    }
    
    public void iniciarVista() {
        vista.setVisible(true);
        vista.setTitle("Docente Pago");
        vista.setSize(500, 600);
    }
    
    public void limpiar() {
        vista.txtNumDocente.setText("");
        vista.txtNombre.setText("");
        vista.txtDomicilio.setText("");
        vista.comboBoxNivel.setSelectedIndex(0);
        vista.txtPagoBasePorHora.setText("");
        vista.txtHorasImpartidas.setText("");
        
        vista.spinnerHijos.setValue(0);
        vista.labelPagoPorHorasImpartidas.setText("0");
        vista.labelBono.setText("0");
        vista.labelDescuentoImpuesto.setText("0");
        vista.labelTotalPagar.setText("0");
    }
    
}
